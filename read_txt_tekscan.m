[FileName,PathName,~]=uigetfile('*.asf','Wähle .asf Datei aus','MultiSelect','on');
% Open filebrowser and select different tests manually

v_tekscan = 0.0409;
% Velocity Tekscansheet with respect to the tire

R= [];
% Initiate R for structure R. Used for data about reliability and reproducibility

for i=1:numel(FileName)

    filename=strcat(PathName,FileName);
    function [TSmtrx,TKS,TEKSCAN_INFO] = read_txt_tekscan(filename)
% filename
% Script to convert TekScan ASCII data to matlab data.
% The configuration of the TekScan software should be:
%
% Number of Frames: 250 [-]
% Seconds per Frame: 0.04 [s]
%
% HOW TO USE
%
% Enter: read_txt_tekscan(filename)
% to start the conversion.
%
% INPUT
%
% An ASCII file containing data from a TekScan movie
%
% OUTPUT
%
% M_movie, containing the footprint in a 3D matrix [mxnxp]
% m = length of the footprint
% n = width of the footprint
% p = frame number (time step)
% 1 output unit = 0.1 [Bar]

fid = fopen(filename);
errmsg = '';
while fid < 0
   disp(errmsg);
   filename = input('Open file: ', 's');
   [fid,errmsg] = fopen(filename);
end
% Open filename for read access
tline ='a';
% Tline may not be empty at the beginning, so the value 'a' is arbitrary
p = 1;
% Start at Frame 1

% Preallocating memory for variable
%41
str = sprintf('open %s', char(filename));
TEKSCAN_INFO = [];
% Cell array of strings for information about test
disp(str)
% ***********************************************************************
% ***********************************************************************
while isempty(tline) || ( ~isempty(tline) && ~strcmp(tline(1),'@') )
% When a '@' is read, the end of the file is reached
    if isempty(tline)
        tline = fgetl(fid);
% Line with text: Frame (number), go to next line
        %tline = fgetl(fid);
% Read first line of data
        m = 1;
        while ~isempty(strfind(tline,',')) % . vervangen door ,
% As long as there is data in tline
            tline = strrep(tline,',',' ');
% Change the seperation comma into an empty space.
            movie(m,:,p) = sscanf(tline,'%f',[1,192]);
% Write data into variable 'movie' (%f = floating point numbers)
            m = m + 1;
% Next m (is next line of sensels)
            tline = fgetl(fid);
% Read next line of data
        end
        % End while
        p = p + 1;
        % Next p (is next frame (timestep))
    else
        [tline,lt] = fgets(fid);
    % Read empty line
        TEKSCAN_INFO = char(TEKSCAN_INFO,tline);
    % Store test infomartion in info matrix
        parameters_temp = sscanf(tline,'%s %*f');
    % Temporary parameter storage
        switch parameters_temp
            case 'ROWS'
                TKS.ROWS = sscanf(tline,'%*s %f');
            % Tekscan rows
            case 'COLS'
                TKS.COLS = sscanf(tline,'%*s %f');
            % Tekscan columns
            case 'ROW_SPACING'
                TKS.ROW_SPACING = sscanf(tline,'%*s %f')/1000;
            % Space between rows [m]
            case 'COL_SPACING'
                TKS.COL_SPACING = sscanf(tline,'%*s %f')/1000;
            % Space between columns [m] 42
            case 'SENSEL_AREA'
                TKS.SENSEL_AREA = sscanf(tline,'%*s %f')/1000^2;
            % Sense area [m^2]
                if sscanf(tline,'%*s %*f %s')=='inÂ'
                    % Test for inches or mm and change units
                    TKS.SENSEL_AREA = TKS.SENSEL_AREA*25.4^2;
                    TKS.ROW_SPACING = TKS.ROW_SPACING*25.4;
                    TKS.COL_SPACING = TKS.COL_SPACING*25.4;
                end
            % end for
            case 'NOISE_THRESHOLD'
                TKS.TRESHOLD = sscanf(tline,'%*s %f');
            % Noise treshold used by Tekscan
            case 'SECONDS_PER_FRAME'
                TKS.SAMPLETIME = sscanf(tline,'%*s %f');
            % Seconds per frame
            case 'Bandmaat:'
                TKS.TIREWIDTH = sscanf(tline,'%*s %f');
            % Tirewidth as in normal tiredimensions
                TKS.TIREHEIGHT = sscanf(tline,'%*s %*f %*c %f');
            % Tireheight as in normal tiredimensions. In precentage (as usual)
                TKS.TIRERADIUS = sscanf(tline,'%*s %*f %*c %*f %*s %f');
            % Tireradius as in normal tiredimensions
            case 'Belastung:'
                TKS.LOAD = sscanf(tline,'%*s %f');
            % Load [kg]
            case 'START_FRAME'
                TKS.FFRAME = sscanf(tline,'%*s %f');
            % First frame (in principal always 1)
            case 'END_FRAME'
                TKS.FRAMES = sscanf(tline,'%*s %f');
            % Last frame (also refered to as number of frames)
            otherwise
    % Just continue when no case is true
        end
    % End switch
    end
    % End if
end
% End while
fclose(fid);
% Close file
TSmtrx = permute(movie,[2,1,3]);
% Reshape matrix
disp('TSmtrx')
% TSmtrx = uint8( floor(TSmtrx * 10));
% Convert to uint8 for memory reasons, Output: 1 [unit] = 0.1 [Bar]
end
% End function
