close all, clear all  clc

% get list of folders  in a structure
folders = dir('C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\01_Shoulder_Instability\TekscanData');
%dirFlags = [folders.isdir]; % Get a logical vector that tells which is a directory.
%subFolders = folders([folders.isdir]); % Extract only those that are directories.
dataFolder = 'C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\01_Shoulder_Instability\TekscanData';
cd (dataFolder);

mapFiles = dir('**/*.asf'); % To get a list of all .asf files, also in subfolders, use **/*


% make a list of specimens for later creation of 3D matrix
for id = 1:length(folders)
    SpecimenNr = folders(id).name(4:end);
    SpecimenList{id,:} = SpecimenNr;
    
end

%% Options for table created for imported .asf files

opts = delimitedTextImportOptions("NumVariables", 24);

% Specify range and delimiter
opts.DataLines = [28, Inf]; % starts after header
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["DATA_TYPEMOVIE", "VarName2", "VarName3", "VarName4", "VarName5", "VarName6", "VarName7", "VarName8", "VarName9", "VarName10", "VarName11", "VarName12", "VarName13", "VarName14", "VarName15", "VarName16", "VarName17", "VarName18", "VarName19", "VarName20", "VarName21", "VarName22", "VarName23", "VarName24"];
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
%opts.EmptyLineRule = "skip";
opts.MissingRule = 'omitrow';


%% Ititialize  variables

%Intact_A_zero = [] % gives so many variables, try to extract from structure (and removing L/R field)

% sensor area (in mm^2) calculation --> change depending on sensor
mh = 41.9; % Matrix Height (MH) in mm found on sensor datasheet
mw = 45.7; % Matrix width (MH) in mm found on sensor datasheet
     
sensor_area = mh*mw ;  %mm^2

% for arrays with max later on
max_vect = {};
intact_max = {};
bankart_max = {};
defect_max = {};
block_max = {};
jbone_max = {};

varNames1 = {'Specimen Nr.', "Test Performed A", 'Mean Max Pressure Pre-test (MPa)','St. Dev. Pre Test',...
    'Mean Max Pressure during Test (MPa)', 'St. Dev. During Test', 'Mean Contact Area (mm^2)'};

varNames = {'Specimen Nr.', 'Protocol' 'Contact Area A(mm)','Mean Max Pressure (MPa)'};
    
savefolder = 'C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\01_Shoulder_Instability\TekscanData'


%variables used to concatenate data from each glenoid condition in array later

i_row = 1;
b_row = 1;
bd_row = 1;
bb_row = 1;
jb_row = 1;

a_row = 1;
pb_row = 1;
%% Data processing (creating structures, arrays,...)
data_all = struct();

for i = 1:numel(mapFiles)
    
   
    
%     [FileName,PathName,~]= fopen(mapFiles(i).name);
%     if ~mapFiles(i).isdir
%         disp(mapFiles(i).name);
%     end

    %disp(mapFiles(i).name);
    dataPath = [mapFiles(i).folder,  '\',mapFiles(i).name];
    pressureMap = table2array(readtable(dataPath, opts)); %directly converting table to array
    
    
    
    % separate frames in 3D matrix sheets
    j = 1; %initialize j, used to go through all blocks/frames of original data and store frames in single sheets in 3D matrix
    nframes = length(pressureMap)/22;
        
    for f = 1:1:nframes % max frames 100, changes depending on tekscan settings
        data_frames (:,:,f) =  pressureMap(j:j+21,:); 
        j = j + 22;
    end
  
    
    % find frame containing peak value (maximum)--> maybe ebtter to calculate mean of all nonzero values and find maximum value of all these. Problem is how to link back to frame with highest mean forarea calculation
    Mall = max(data_frames,[],[1 2]); % maximum in each frame, same as maxval so can edit code to simplify later on
    % extract maximum of 1st frame, i.e. pre-test pressure
    max_pre = Mall(:,:,1);
    maxOverall = max(Mall,[],'all'); % overall max value(peak)
    [s1, s2, s3] = size(data_frames);
    [maxval, ind] = max(reshape(data_frames(:), s1*s2, []));
    max_idx = find(maxval == maxOverall); % returns index of frames where peak is. Now either take mean of all max frames or take first --> problem with mean if not reproducible measurement
    max_frame = data_frames(:,:,max_idx(1,1)); %taking first occurring max 
    
    all_test_max(:,:,i) = max_frame; % max frames of all performed tests, new test in 3rd dimension
    all_maxval(:,:,i) = maxOverall;
    

    % calculate contact area in mm^2 at max frame, based on % of non-zero to total values. Change method if not taking max value in frame but mean of all sensels
    size_sens = size(max_frame); 
    tot_val = size_sens(1)*size_sens(2); %total number of values
    non_zero = nnz(max_frame);
    coverage = non_zero/tot_val; % percentage of non zero values to total values, i.e. percentage of sensor coverage
    nnz_pressure = find(
    contact_a = coverage*sensor_area; %contact area in mm^2
    
   
    
    % Order all 3D matrices in a structure and separate by protocol, angle etc
     
    ch_name = mapFiles(i).name;
    a = find(ismember(ch_name, '_'));
    ch_state = ch_name(1:a(1)-1);
    ch_protocol = ch_name(a(1)+1:a(2)-1);
    
    if ch_name(a(2)+1:a(3)-1) == '0'
       ch_deg = 'zero';
       
    elseif ch_name(a(2)+1:a(3)-1)== '10'
       ch_deg = 'ten';
       
    elseif ch_name(a(2)+1:a(3)-1)== '20'
       ch_deg = 'twenty';
       
    end
    
    ch_folder = mapFiles(i).folder;
    
    ch_side = ch_folder(108);

    
%        
%     if ch_name(a(3)+1:end-4)== '1'
%        ch_rep = 'one';
%        
%     elseif ch_name(a(3)+1:end-4)== '2'
%        ch_rep = 'two';
%        
%     elseif ch_name(a(3)+1:end-4)== '3'
%        ch_rep = 'three';
%        
%     end
    
        
    
 
        if isfield(data_all, (ch_state)) && isfield(data_all.(ch_state), (ch_protocol)) ...
            && isfield(data_all.(ch_state).(ch_protocol), (ch_deg)) ...
            && isfield(data_all.(ch_state).(ch_protocol).(ch_deg), (ch_side)) ...
            && isfield(data_all.(ch_state).(ch_protocol).(ch_deg).(ch_side), 'data')
        
                t = length(data_all.(ch_state).(ch_protocol).(ch_deg).(ch_side)) + 1;
        else 
                t = 1;
        end
        data_all.(ch_state).(ch_protocol).(ch_deg).(ch_side)(t).data = data_frames;
        data_all.(ch_state).(ch_protocol).(ch_deg).(ch_side)(t).maxFrame = max_frame;
        data_all.(ch_state).(ch_protocol).(ch_deg).(ch_side)(t).maxPre = max_pre;
        data_all.(ch_state).(ch_protocol).(ch_deg).(ch_side)(t).maxTest = maxOverall;
        
        clear data_frames %ch_name ch_protocol ch_side ch_state ch_deg

        
        % Generate array with maximum values per specimen --> needed for tables
         
        
        specimen_nr = mapFiles(i).folder(96:114);
        
        max_vect{i,1} = specimen_nr;
        max_vect{i,2} = ch_name(1:end-6); %name of test and protocol
        max_vect{i,3} = max_pre; 
        max_vect{i,4} = maxOverall;
        max_vect{i,5} = contact_a; %contact area
        
        % arrays by glenoid state
         
    if ch_state == 'I'
     
       intact_max{i_row,1} = specimen_nr;
       intact_max{i_row,2} = ch_name(1:end-6);
       intact_max{i_row,3} = max_pre;
       intact_max{i_row,4} = maxOverall;
       intact_max{i_row,5} = contact_a;
       
       i_row = i_row + 1;
       
    end
    
    if ch_state == 'B' 
        
       bankart_max{b_row,1} = specimen_nr;
       bankart_max{b_row,2} = ch_name(1:end-6);
       bankart_max{b_row,3} = max_pre;
       bankart_max{b_row,4} = maxOverall;
       bankart_max{b_row,5} = contact_a;
       
       b_row = b_row + 1;
       
    end
    
    if ch_state == 'BD'
        
       defect_max{bd_row,1} = specimen_nr;
       defect_max{bd_row,2} = ch_name(1:end-6);
       defect_max{bd_row,3} = max_pre;
       defect_max{bd_row,4} = maxOverall;
       defect_max{bd_row,5} = contact_a;
       
       bd_row = bd_row + 1;
       
    end
       
    if ch_state == 'BB'  
        
       block_max{bb_row,1} = specimen_nr;
       block_max{bb_row,2} = ch_name(1:end-6); 
       block_max{bb_row,3} = max_pre;
       block_max{bb_row,4} = maxOverall;
       block_max{bb_row,5} = contact_a;
       
       bb_row = bb_row + 1;
       
    end
    
    if ch_state == 'JB'
       
       jbone_max{jb_row,1} = specimen_nr;
       jbone_max{jb_row,2} = ch_name(1:end-6);
       jbone_max{jb_row,3} = max_pre;
       jbone_max{jb_row,4} = maxOverall;
       jbone_max{jb_row,5} = contact_a;
       
       jb_row = jb_row + 1;
       
    end
        
    % arrays by protocol --> for tables later
    
    % Protocol A
         
    if ch_name(1:3) == 'I_A' 
        
       pA_max{a_row,1} = specimen_nr;
       pA_max{a_row,2} = ch_name(1:end-6);
       pA_max{a_row,3} = maxOverall;
       
       a_row = a_row + 1; 
       
    elseif ch_name(1:3) == 'B_A' 
       
       pA_max{a_row,1} = specimen_nr;
       pA_max{a_row,2} = ch_name(1:end-6); 
       pA_max{a_row,4} = maxOverall;

       a_row = a_row + 1; 
       
    elseif ch_name(1:4) == 'BD_A' 
       
       pA_max{a_row,1} = specimen_nr;
       pA_max{a_row,2} = ch_name(1:end-6);
       pA_max{a_row,5} = maxOverall;
       
       a_row = a_row + 1; 
       
    elseif ch_name(1:4) == 'BB_A' 
       
       pA_max{a_row,1} = specimen_nr;
       pA_max{a_row,2} = ch_name(1:end-6); 
       pA_max{a_row,6} = maxOverall;
       
       a_row = a_row + 1; 
       
    elseif ch_name(1:4) == 'JB_A'
       
       pA_max{a_row,1} = specimen_nr;
       pA_max{a_row,2} = ch_name(1:end-6); 
       pA_max{a_row,7} = maxOverall;
       
       a_row = a_row + 1;    
    
    end
    
    
    % protocol B
    
     if ch_name(1:3) == 'I_B' 
        
       pB_max{pb_row,1} = specimen_nr;
       pB_max{pb_row,2} = ch_name(1:end-6);
       pB_max{pb_row,3} = maxOverall; % column for intact
       
       pb_row = pb_row + 1; 
       
    elseif ch_name(1:3) == 'B_B' 
       
       pB_max{pb_row,1} = specimen_nr;
       pB_max{pb_row,2} = ch_name(1:end-6); 
       pB_max{pb_row,4} = maxOverall; % column for bankart

       pb_row = pb_row + 1; 
       
    elseif ch_name(1:4) == 'BD_B' 
       
       pB_max{pb_row,1} = specimen_nr;
       pB_max{pb_row,2} = ch_name(1:end-6);
       pB_max{pb_row,5} = maxOverall; % column for bone defect
      
       pb_row = pb_row + 1; 
       
    elseif ch_name(1:4) == 'BB_B' 
       
       pB_max{pb_row,1} = specimen_nr;
       pB_max{pb_row,2} = ch_name(1:end-6); 
       pB_max{pb_row,6} = maxOverall; % column for block
       
       pb_row = pb_row + 1; 
       
    elseif ch_name(1:4) == 'JB_B'
       
       pB_max{pb_row,1} = specimen_nr;
       pB_max{pb_row,2} = ch_name(1:end-6); 
       pB_max{pb_row,7} = maxOverall; % column for jbone
       
       pb_row = pb_row + 1;    
    
    end
       
        
end

%% Calculate means of 3 experiments and create table with all specimen
% maybe better to store the values in a temporary variable in the loop above, to have less lines of code. Or use cell matrix

% Table with all specimen and all glenoid conditions
   

 v = 1;
 
for idx = 1:3:length(max_vect) 
    
        mean_arr{v,1} = max_vect{idx,1}; %specimen nr
        mean_arr{v,2} = max_vect{idx,2}; % test performed
        mean_arr{v,3} = round(mean([max_vect{idx:idx+2,3}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum pre-test
        mean_arr{v,4} = round(std([max_vect{idx:idx+2,3}]), 2);
        mean_arr{v,5} = round(mean([max_vect{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum
        mean_arr{v,6} = round(std([max_vect{idx:idx+2,4}]), 2); 
        mean_arr{v,7} = round(mean([max_vect{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean cotact area of 3 tests
        
        v = v + 1;
    %end
end

mean_table_spec = [varNames1; mean_arr];
cd (savefolder)
writetable(table(mean_table_spec), 'Pressure_Means_AllSpecimen.xls');



%% Tables separated by glenoid condition

% Intact

 v = 1;
 
for idx = 1:3:length(intact_max) 
    
        intact_arr{v,1} = intact_max{idx,1}; %specimen nr
        intact_arr{v,2} = intact_max{idx,2}; % test performed
        intact_arr{v,3} = round(mean([intact_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % contact area mean
        intact_arr{v,4} = round(mean([intact_max{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum
        
        v = v + 1;
    
end

intact_table_means = [varNames; intact_arr];
cd (savefolder)
writetable(table(intact_table_means), 'Intact_MeanPressure.xls');

% Bankart

 v = 1;
 
for idx = 1:3:length(bankart_max) 
    
        bankart_arr{v,1} = bankart_max{idx,1}; %specimen nr
        bankart_arr{v,2} = bankart_max{idx,2}; % test performed
        bankart_arr{v,3} = round(mean([bankart_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % contact area mean
        bankart_arr{v,4} = round(mean([bankart_max{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum
        
        v = v + 1;
    
end

bankart_table_means = [varNames; bankart_arr];
cd (savefolder)
writetable(table(bankart_table_means), 'Bankart_MeanPressure.xls');

% Bone Defect

 v = 1;
 
for idx = 1:3:length(defect_max) 
    
        defect_arr{v,1} = defect_max{idx,1}; %specimen nr
        defect_arr{v,2} = defect_max{idx,2}; % test performed
        defect_arr{v,3} = round(mean([defect_max{idx:idx+2,5}], 'native', 'omitnan'), 2); 
        defect_arr{v,4} = round(mean([defect_max{idx:idx+2,4}], 'native', 'omitnan'), 2);
        
        v = v + 1
    
end

defect_table_means = [varNames; defect_arr];
cd (savefolder)
writetable(table(defect_table_means), 'Defect_MeanPressure.xls');

% Bone Block

 v = 1;
 
for idx = 1:3:length(block_max) 
    
        block_arr{v,1} = block_max{idx,1}; %specimen nr
        block_arr{v,2} = block_max{idx,2}; % test performed
        block_arr{v,3} = round(mean([block_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum pre-test
        block_arr{v,4} = round(mean([block_max{idx:idx+2,4}], 'native', 'omitnan'), 2);

        
        v = v + 1;
    %end
end

block_table_means = [varNames; block_arr];
cd (savefolder)
writetable(table(block_table_means), 'Block_MeanPressure.xls');

% J-bone

 v = 1;
 
for idx = 1:3:length(jbone_max) 
    
        jbone_arr{v,1} = jbone_max{idx,1}; %specimen nr
        jbone_arr{v,2} = jbone_max{idx,2}; % test performed
        jbone_arr{v,3} = round(mean([jbone_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum pre-test
        jbone_arr{v,4} = round(mean([jbone_max{idx:idx+2,4}], 'native', 'omitnan'), 2);
 
        
        v = v + 1;
    %end
end

jbone_table_means = [varNames; jbone_arr];
cd (savefolder)
writetable(table(jbone_table_means), 'J-bone_MeanPressure.xls');


%% table of maximum values for protocol A

% Protocol A

varNamesA = {'Specimen Nr.', 'Test Performed','Intact Mean (MPa)', 'SD Intact', ...
    'Bankart Mean (MPa)','SD Bankart', 'Defect Mean(MPa)', 'SD Defect', 'Block Mean(MPa)','SD Block', 'J-Bone Mean (MPa)', 'SD J-Bone'};
 v = 1;
 
for idx = 1:3:length(pA_max) 
    
        A_arr{v,1} = pA_max{idx,1}; %specimen nr
        A_arr{v,2} = pA_max{idx,2}; % test performed
        A_arr{v,3} = round(mean([pA_max{idx:idx+2,3}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum intact
        A_arr{v,4} = round(std([pA_max{idx:idx+2,3}], 'omitnan'), 2);
        A_arr{v,5} = round(mean([pA_max{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bankart
        A_arr{v,6} = round(std([pA_max{idx:idx+2,4}], 'omitnan'), 2);
        A_arr{v,7} = round(mean([pA_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bone defect
        A_arr{v,8} = round(std([pA_max{idx:idx+2,5}], 'omitnan'), 2);
        A_arr{v,9} = round(mean([pA_max{idx:idx+2,6}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bone block
        A_arr{v,10} = round(std([pA_max{idx:idx+2,6}], 'omitnan'), 2);
        A_arr{v,11} = round(mean([pA_max{idx:idx+2,7}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum j-bone
        A_arr{v,12} = round(std([pA_max{idx:idx+2,7}], 'omitnan'), 2);
        
        v = v + 1;
    %end
end

A_table_means = [varNamesA; A_arr];
cd (savefolder)
writetable(table(A_table_means), 'A-Protocol_MeanPressure.xls');

%% Table of maximum values for protocol B

varNamesA = {'Specimen Nr.', 'Test Performed','Intact Mean (MPa)', 'SD Intact', ...
    'Bankart Mean (MPa)','SD Bankart', 'Defect Mean(MPa)', 'SD Defect', 'Block Mean(MPa)','SD Block', 'J-Bone Mean (MPa)', 'SD J-Bone'};
 v = 1;
 
for idx = 1:3:length(pB_max) 
    
        B_arr{v,1} = pB_max{idx,1}; %specimen nr
        B_arr{v,2} = pB_max{idx,2}; % test performed
        B_arr{v,3} = round(mean([pB_max{idx:idx+2,3}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum intact
        B_arr{v,4} = round(std([pB_max{idx:idx+2,3}], 'omitnan'), 2);
        B_arr{v,5} = round(mean([pB_max{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bankart
        B_arr{v,6} = round(std([pB_max{idx:idx+2,4}], 'omitnan'), 2);
        B_arr{v,7} = round(mean([pB_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bone defect
        B_arr{v,8} = round(std([pB_max{idx:idx+2,5}], 'omitnan'), 2);
        B_arr{v,9} = round(mean([pB_max{idx:idx+2,6}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bone block
        B_arr{v,10} = round(std([pB_max{idx:idx+2,6}], 'omitnan'), 2);
        B_arr{v,11} = round(mean([pB_max{idx:idx+2,7}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum j-bone
        B_arr{v,12} = round(std([pB_max{idx:idx+2,7}], 'omitnan'), 2);
        
        v = v + 1;
    %end
end

B_table_means = [varNamesA; B_arr];
cd (savefolder)
writetable(table(B_table_means), 'B-Protocol_MeanPressure.xls');

%% Trying new structure to calculate mean and sepatate

% 
% mean_data = struct()
% tt = 1
% 
% for ii = 1:3:18
%     
%     disp ([data_all.I.A.zero.R(ii:ii+2).maxTest]);
%     mean_data.I.A.zero.R(tt).data = mean([data_all.I.A.zero.R(ii:ii+2).maxTest], 'all', 'omitnan');
%     
%     tt = length(mean_data.I.A.zero.R) + 1
%     
% end
% 
% histogram(cell2mat(A_table_means(2:4, 3)), 'Labels', {'0 deg', '-10 deg','-20 deg'})