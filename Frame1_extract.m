close all, clear all  clc

% get list of folders  in a structure
folders = dir('C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\01_Shoulder_Instability\TekscanData');
%dirFlags = [folders.isdir]; % Get a logical vector that tells which is a directory.
%subFolders = folders([folders.isdir]); % Extract only those that are directories.
dataFolder = 'C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\01_Shoulder_Instability\TekscanData';
cd (dataFolder);

mapFiles = dir('**/*.asf'); % To get a list of all .asf files, also in subfolders, use **/*


% make a list of specimens for later creation of 3D matrix
% for id = 1:length(folders)
%     SpecimenNr = folders(id).name(4:end);
%     SpecimenList{id,:} = SpecimenNr;
%     
% end

%% Options for table created for imported .asf files

opts = delimitedTextImportOptions("NumVariables", 24);

% Specify range and delimiter
opts.DataLines = [28, Inf]; % starts after header
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["DATA_TYPEMOVIE", "VarName2", "VarName3", "VarName4", "VarName5", "VarName6", "VarName7", "VarName8", "VarName9", "VarName10", "VarName11", "VarName12", "VarName13", "VarName14", "VarName15", "VarName16", "VarName17", "VarName18", "VarName19", "VarName20", "VarName21", "VarName22", "VarName23", "VarName24"];
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
%opts.EmptyLineRule = "skip";
opts.MissingRule = 'omitrow';


%% Ititialize  variables


% sensor area (in mm^2) calculation --> change depending on sensor
mh = 41.9; % Matrix Height (MH) in mm found on sensor datasheet
mw = 45.7; % Matrix width (MH) in mm found on sensor datasheet
     
sensor_area = mh*mw ;  %mm^2

% for arrays with max later on
max_vect = {};
intact_max = {};
bankart_max = {};
defect_max = {};
block_max = {};
jbone_max = {};

varNames1 = {'Specimen Nr.', "Test Performed A", 'Mean Max Pressure Pre-test (MPa)','St. Dev. Pre Test',...
    'Mean Max Pressure during Test (MPa)', 'St. Dev. During Test', 'Mean Contact Area (mm^2)'};

varNames = {'Specimen Nr.','Protocol','Max Pressure (MPa)','Contact Area (mm)','Contact Pressure (MPa'};
    
savefolder = 'C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\01_Shoulder_Instability\01_TekscanNew'


%variables used to concatenate data from each glenoid condition in array later

i_row = 1;
b_row = 1;
bd_row = 1;
bb_row = 1;
jb_row = 1;

a_row = 1;
pb_row = 1;
%% Data processing (creating structures, arrays,...)
data_all = struct();

for i = 1:numel(mapFiles)
    

    %disp(mapFiles(i).name);
    dataPath = [mapFiles(i).folder,  '\',mapFiles(i).name];
    pressureMap = table2array(readtable(dataPath, opts)); %directly converting table to array
    
    
    
    % separate frames in 3D matrix sheets
    j = 1; %initialize j, used to go through all blocks/frames of original data and store frames in single sheets in 3D matrix
    nframes = length(pressureMap)/22;
        
    for f = 1:1:nframes % max frames 100, changes depending on tekscan settings
        data_frames (:,:,f) =  pressureMap(j:j+21,:); 
        j = j + 22;
    end
  
    
    % find frame containing peak value (maximum)--> maybe better to calculate mean of all nonzero values and find maximum value of all these. Problem is how to link back to frame with highest mean forarea calculation
    Mall = max(data_frames,[],[1 2]); % maximum in each frame, same as maxval so can edit code to simplify later on
    % extract maximum of 1st frame, i.e. pre-test pressure
    max_one = Mall(:,:,1);
    frame_one = data_frames(:,:,1); %first frame
    all_test_max(:,:,i) = frame_one; % First frame of all performed tests, new test in 3rd dimension
    all_maxval(:,:,i) = max_one;
    

    % calculate contact area in mm^2 at max frame, based on % of non-zero to total values. Change method if not taking max value in frame but mean of all sensels
    size_sens = size(frame_one); 
    tot_val = size_sens(1)*size_sens(2); %total number of values
    non_zero = nnz(frame_one);
    coverage = non_zero/tot_val; % percentage of non zero values to total values, i.e. percentage of sensor coverage
    contact_a = round(coverage*sensor_area,1); %contact area in mm^2
    tot_p = sum(frame_one, 'all'); % sum of pressure in all sensels
    contact_p = round(tot_p/non_zero, 2);%contact pressure as out of the tekscan software is given by this formula
    
    
   
        
        % Generate array with maximum values per specimen --> needed for tables
         
        ch_name = mapFiles(i).name;  
        a = find(ismember(ch_name, '_'));
        ch_state = ch_name(1:a(1)-1);
        ch_protocol = ch_name(a(1)+1:a(2)-1);
        specimen_nr = mapFiles(i).folder(96:114);
        
        max_vect{i,1} = specimen_nr;
        max_vect{i,2} = ch_name(1:end-4); %name of test and protocol
        max_vect{i,3} = max_one; 
        max_vect{i,4} = contact_a; %contact area
        max_vect{i,5} = contact_p;
        
        

        
        % arrays by glenoid state
         
    if ch_state == 'I'
     
       intact_max{i_row,1} = specimen_nr;
       intact_max{i_row,2} = ch_name(1:end-4);
       intact_max{i_row,3} = max_one;
       intact_max{i_row,4} = contact_a;
       intact_max{i_row,5} = contact_p;
       i_row = i_row + 1;
       
    end
    
    if ch_state == 'B' 
        
       bankart_max{b_row,1} = specimen_nr;
       bankart_max{b_row,2} = ch_name(1:end-4);
       bankart_max{b_row,3} = max_one;
       bankart_max{b_row,4} = contact_a;
       bankart_max{b_row,5} = contact_p;
       
       b_row = b_row + 1;
       
    end
    
    if ch_state == 'BD'
        
       defect_max{bd_row,1} = specimen_nr;
       defect_max{bd_row,2} = ch_name(1:end-4);
       defect_max{bd_row,3} = max_one;
       defect_max{bd_row,4} = contact_a;
       defect_max{bd_row,5} = contact_p;
       
       bd_row = bd_row + 1;
       
    end
       
    if ch_state == 'BB'  
        
       block_max{bb_row,1} = specimen_nr;
       block_max{bb_row,2} = ch_name(1:end-4); 
       block_max{bb_row,3} = max_one;
       block_max{bb_row,4} = contact_a;
       block_max{bb_row,5} = contact_p;
       
       bb_row = bb_row + 1;
       
    end
    
    if ch_state == 'JB'
       
       jbone_max{jb_row,1} = specimen_nr;
       jbone_max{jb_row,2} = ch_name(1:end-4);
       jbone_max{jb_row,3} = max_one;
       jbone_max{jb_row,4} = contact_a;
       jbone_max{jb_row,5} = contact_p;
       
       jb_row = jb_row + 1;
       
    end
        
       
        
end

%% Excel table with pressure information
% maybe better to store the values in a temporary variable in the loop above, to have less lines of code. Or use cell matrix

% Table with all specimen and all glenoid conditions for frame 1
   
        table_allF1 = [varNames; max_vect];
        cd (savefolder)
        writetable(table(table_allF1), 'Frame1_AllSpecimen.xls');

%  v = 1;
%  
% for idx = 1:3:length(max_vect) 
%     
%         mean_arr{v,1} = max_vect{idx,1}; %specimen nr
%         mean_arr{v,2} = max_vect{idx,2}; % test performed
%         mean_arr{v,3} = round(mean([max_vect{idx:idx+2,3}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum pre-test
%         mean_arr{v,4} = round(std([max_vect{idx:idx+2,3}]), 2);
%         mean_arr{v,5} = round(mean([max_vect{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum
%         mean_arr{v,6} = round(std([max_vect{idx:idx+2,4}]), 2); 
%         mean_arr{v,7} = round(mean([max_vect{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean cotact area of 3 tests
%         
%         v = v + 1;
%     %end
% end
% 
% mean_table_spec = [varNames1; mean_arr];
% cd (savefolder)
% writetable(table(mean_table_spec), 'Pressure_Means_AllSpecimen.xls');
% 
% 
% 
% %% Tables separated by glenoid condition
% 
% % Intact
% 
%  v = 1;
%  
% for idx = 1:3:length(intact_max) 
%     
%         intact_arr{v,1} = intact_max{idx,1}; %specimen nr
%         intact_arr{v,2} = intact_max{idx,2}; % test performed
%         intact_arr{v,3} = round(mean([intact_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % contact area mean
%         intact_arr{v,4} = round(mean([intact_max{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum
%         
%         v = v + 1;
%     
% end
% 
% intact_table_means = [varNames; intact_arr];
% cd (savefolder)
% writetable(table(intact_table_means), 'Intact_MeanPressure.xls');
% 
% % Bankart
% 
%  v = 1;
%  
% for idx = 1:3:length(bankart_max) 
%     
%         bankart_arr{v,1} = bankart_max{idx,1}; %specimen nr
%         bankart_arr{v,2} = bankart_max{idx,2}; % test performed
%         bankart_arr{v,3} = round(mean([bankart_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % contact area mean
%         bankart_arr{v,4} = round(mean([bankart_max{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum
%         
%         v = v + 1;
%     
% end
% 
% bankart_table_means = [varNames; bankart_arr];
% cd (savefolder)
% writetable(table(bankart_table_means), 'Bankart_MeanPressure.xls');
% 
% % Bone Defect
% 
%  v = 1;
%  
% for idx = 1:3:length(defect_max) 
%     
%         defect_arr{v,1} = defect_max{idx,1}; %specimen nr
%         defect_arr{v,2} = defect_max{idx,2}; % test performed
%         defect_arr{v,3} = round(mean([defect_max{idx:idx+2,5}], 'native', 'omitnan'), 2); 
%         defect_arr{v,4} = round(mean([defect_max{idx:idx+2,4}], 'native', 'omitnan'), 2);
%         
%         v = v + 1
%     
% end
% 
% defect_table_means = [varNames; defect_arr];
% cd (savefolder)
% writetable(table(defect_table_means), 'Defect_MeanPressure.xls');
% 
% % Bone Block
% 
%  v = 1;
%  
% for idx = 1:3:length(block_max) 
%     
%         block_arr{v,1} = block_max{idx,1}; %specimen nr
%         block_arr{v,2} = block_max{idx,2}; % test performed
%         block_arr{v,3} = round(mean([block_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum pre-test
%         block_arr{v,4} = round(mean([block_max{idx:idx+2,4}], 'native', 'omitnan'), 2);
% 
%         
%         v = v + 1;
%     %end
% end
% 
% block_table_means = [varNames; block_arr];
% cd (savefolder)
% writetable(table(block_table_means), 'Block_MeanPressure.xls');
% 
% % J-bone
% 
%  v = 1;
%  
% for idx = 1:3:length(jbone_max) 
%     
%         jbone_arr{v,1} = jbone_max{idx,1}; %specimen nr
%         jbone_arr{v,2} = jbone_max{idx,2}; % test performed
%         jbone_arr{v,3} = round(mean([jbone_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum pre-test
%         jbone_arr{v,4} = round(mean([jbone_max{idx:idx+2,4}], 'native', 'omitnan'), 2);
%  
%         
%         v = v + 1;
%     %end
% end
% 
% jbone_table_means = [varNames; jbone_arr];
% cd (savefolder)
% writetable(table(jbone_table_means), 'J-bone_MeanPressure.xls');


%% table of maximum values for protocol A
% 
% % Protocol A
% 
% varNamesA = {'Specimen Nr.', 'Test Performed','Intact Mean (MPa)', 'SD Intact', ...
%     'Bankart Mean (MPa)','SD Bankart', 'Defect Mean(MPa)', 'SD Defect', 'Block Mean(MPa)','SD Block', 'J-Bone Mean (MPa)', 'SD J-Bone'};
%  v = 1;
%  
% for idx = 1:3:length(pA_max) 
%     
%         A_arr{v,1} = pA_max{idx,1}; %specimen nr
%         A_arr{v,2} = pA_max{idx,2}; % test performed
%         A_arr{v,3} = round(mean([pA_max{idx:idx+2,3}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum intact
%         A_arr{v,4} = round(std([pA_max{idx:idx+2,3}], 'omitnan'), 2);
%         A_arr{v,5} = round(mean([pA_max{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bankart
%         A_arr{v,6} = round(std([pA_max{idx:idx+2,4}], 'omitnan'), 2);
%         A_arr{v,7} = round(mean([pA_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bone defect
%         A_arr{v,8} = round(std([pA_max{idx:idx+2,5}], 'omitnan'), 2);
%         A_arr{v,9} = round(mean([pA_max{idx:idx+2,6}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bone block
%         A_arr{v,10} = round(std([pA_max{idx:idx+2,6}], 'omitnan'), 2);
%         A_arr{v,11} = round(mean([pA_max{idx:idx+2,7}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum j-bone
%         A_arr{v,12} = round(std([pA_max{idx:idx+2,7}], 'omitnan'), 2);
%         
%         v = v + 1;
%     %end
% end
% 
% A_table_means = [varNamesA; A_arr];
% cd (savefolder)
% writetable(table(A_table_means), 'A-Protocol_MeanPressure.xls');

%% Table of maximum values for protocol B
% 
% varNamesA = {'Specimen Nr.', 'Test Performed','Intact Mean (MPa)', 'SD Intact', ...
%     'Bankart Mean (MPa)','SD Bankart', 'Defect Mean(MPa)', 'SD Defect', 'Block Mean(MPa)','SD Block', 'J-Bone Mean (MPa)', 'SD J-Bone'};
%  v = 1;
%  
% for idx = 1:3:length(pB_max) 
%     
%         B_arr{v,1} = pB_max{idx,1}; %specimen nr
%         B_arr{v,2} = pB_max{idx,2}; % test performed
%         B_arr{v,3} = round(mean([pB_max{idx:idx+2,3}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum intact
%         B_arr{v,4} = round(std([pB_max{idx:idx+2,3}], 'omitnan'), 2);
%         B_arr{v,5} = round(mean([pB_max{idx:idx+2,4}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bankart
%         B_arr{v,6} = round(std([pB_max{idx:idx+2,4}], 'omitnan'), 2);
%         B_arr{v,7} = round(mean([pB_max{idx:idx+2,5}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bone defect
%         B_arr{v,8} = round(std([pB_max{idx:idx+2,5}], 'omitnan'), 2);
%         B_arr{v,9} = round(mean([pB_max{idx:idx+2,6}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum bone block
%         B_arr{v,10} = round(std([pB_max{idx:idx+2,6}], 'omitnan'), 2);
%         B_arr{v,11} = round(mean([pB_max{idx:idx+2,7}], 'native', 'omitnan'), 2); % mean of 3 tests for maximum j-bone
%         B_arr{v,12} = round(std([pB_max{idx:idx+2,7}], 'omitnan'), 2);
%         
%         v = v + 1;
%     %end
% end
% 
% B_table_means = [varNamesA; B_arr];
% cd (savefolder)
% writetable(table(B_table_means), 'B-Protocol_MeanPressure.xls');
