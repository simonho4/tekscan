clear all, close all, clc

% Import excel data
data = readtable('C:\Users\simon\polybox\01_Balgrist\02_Research\01_Projects\04_Shoulder-Instability_Hochreiter\3D-Testing\Vector-Tekscan\Vector-Data.xlsx');

coord = table2array(data(:,8:11));
shoulders = table2cell(data(:,1));
% 
% a = find(ismember(shoulders, 'SSM_35'));
 
% row 1 = x, row 2 = y
% for vector sum add data to excel and add rows in variable according to
% data
SSM_35 = coord(1,:);
SSM_60 = coord(2,:);
SSM_75 = coord(3,:);
B1_35 = coord(4,:); 
B1_60 = coord(5,:);
B1_75 = coord(6,:);
B1_Ac_35 = coord(7,:);
B1_Ac_60 = coord(8,:);
B1_Ac_75 = coord(9,:);
B1_Gl_35 = coord(10,:);
B1_Gl_60 = coord(11,:);
B1_Gl_75 = coord(12,:);
DL_35 = coord(13,:);
DL_60 = coord(14,:);
DL_75 = coord(15,:);

u = 0;
v = 0;


LineWidth = 2;
ArrowSize = 0.5;

%% SSM Vectors
vec_SSM = figure (1);
%35°
v1 = quiver(u,v,SSM_35(1),SSM_35(2),0);
v1.LineWidth = LineWidth;
v1.MaxHeadSize = ArrowSize;

hold on

%60°
v2 = quiver(u,v,SSM_60(1),SSM_60(2),0);
v2.LineWidth = LineWidth;
% v2.MaxHeadSize = ArrowSize;


%75°
v3 = quiver(u,v,SSM_75(1),SSM_75(2),0);
v3.LineWidth = LineWidth;
% v3.MaxHeadSize = ArrowSize;


xO = 0.4;  %arrow length
yO = 0.2; % arrow width
arrows = patch([12-xO -yO; 12-xO +yO; 12 0],[yO 7-xO; -yO 7-xO; 0 7], 'k', 'clipping', 'off'); % set position of arrows, depends on Xlim and Ylim

%Options
grid off
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis equal ;
ax.XLim = [-7 12];
ax.YLim = [-12 7];
ax.XTick = [-6:2:11];
ax.YTick = [-11:2:6];


hold off

lgd = legend ([v1 v2 v3],'35°', '60°','75°','Location','bestoutside','Orientation','horizontal', 'box','off','Position', [0.43 0.08 0 0]);
t = title('Dislocation Vectors SSM','FontSize',14, 'Position', [0 8 0]);
xlabel ('Posterior','FontWeight','bold');
ylabel ('Superior','FontWeight','bold');
set (gca, 'box', 'off');
set(gcf,'color','w'); % white background
set(gcf, 'Position', [10 10 700 700]);

%Mark dislocation
annotation(vec_SSM,'textbox',...
    [0.695610534716681 0.260075187969925 0.00399042298483931 0.00501253132832091],...
    'String','*',...
    'LineWidth',8,...
    'FontWeight','bold',...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1],...
    'BackgroundColor',[1 1 1]);

%% B1 Vectors

vec_B1 = figure(2);

%35°
v1 = quiver(u,v,B1_35(1),B1_35(2),0);
v1.LineWidth = LineWidth;
v1.MaxHeadSize = 0.15;

hold on

%60°
v2 = quiver(u,v,B1_60(1),B1_60(2),0);
v2.LineWidth = LineWidth;
% v2.MaxHeadSize = ArrowSize;


%75°
v3 = quiver(u,v,B1_75(1),B1_75(2),0);
v3.LineWidth = LineWidth;
% v3.MaxHeadSize = ArrowSize;


xO = 0.4;  %arrow length
yO = 0.2; % arrow width
arrows = patch([12-xO -yO; 12-xO +yO; 12 0],[yO 7-xO; -yO 7-xO; 0 7], 'k', 'clipping', 'off'); % set position of arrows, depends on Xlim and Ylim

%Options
grid off
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis equal ;
ax.XLim = [-7 12];
ax.YLim = [-11 7];
ax.XTick = [-6:2:11];
ax.YTick = [-10:2:6];


hold off

lgd = legend ([v1 v2 v3],'35°', '60°','75°','Location','southoutside','Orientation','horizontal', 'box','off','Position',[0.43 0.08 0 0]);
t = title('Dislocation Vectors B1','FontSize',14, 'Position', [0 8 0]);
xlabel ('Posterior','FontWeight','bold');
ylabel ('Superior','FontWeight','bold');
set (gca, 'box', 'off');
set(gcf,'color','w'); % white background
set(gcf, 'Position', [10 10 700 700]);

% Mark dislocation


% Create textbox
annotation(vec_B1,'textbox',...
    [0.768621830209482 0.445148141158709 0.00771775082690251 0.00264200792602393],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_B1,'textbox',...
    [0.730050401638053 0.400862426872995 0.00771775082690251 0.00264200792602393],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

%% B1 Acromion Correction

vec_B1Ac = figure (3);

%35°
v1 = quiver(u,v,B1_Ac_35(1),B1_Ac_35(2),0);
v1.LineWidth = LineWidth;
v1.MaxHeadSize = 0.3;

hold on

%60°
v2 = quiver(u,v,B1_Ac_60(1),B1_Ac_60(2),0);
v2.LineWidth = LineWidth;
% v2.MaxHeadSize = ArrowSize;


%75°
v3 = quiver(u,v,B1_Ac_75(1),B1_Ac_75(2),0);
v3.LineWidth = LineWidth;
v3.MaxHeadSize = 0.15;


xO = 0.4;  %arrow length
yO = 0.2; % arrow width
arrows = patch([12-xO -yO; 12-xO +yO; 12 0],[yO 7-xO; -yO 7-xO; 0 7], 'k', 'clipping', 'off'); % set position of arrows, depends on Xlim and Ylim

%Options
grid off
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis equal ;
ax.XLim = [-7 12];
ax.YLim = [-7 7];
ax.XTick = [-6:2:11];
ax.YTick = [-6:2:6];


hold off

lgd = legend ([v1 v2 v3],'35°', '60°','75°','Location','southoutside','Orientation','horizontal', 'box','off','Position',[0.43 0.15 0 0]);
t = title('Dislocation Vectors B1 Acromion Correction','FontSize',14, 'Position', [0 8 0]);
xlabel ('Posterior','FontWeight','bold');
ylabel ('Superior','FontWeight','bold');
set (gca, 'box', 'off');
set(gcf,'color','w'); % white background
set(gcf, 'Position', [10 10 700 700]);

%Mark dislocaiton

% Create textbox
annotation(vec_B1Ac,'textbox',...
    [0.840526315789473 0.427889908256881 0.0141578947368418 0.0201834862385321],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1],...
    'BackgroundColor',[1 1 1]);

% Create textbox
annotation(vec_B1Ac,'textbox',...
    [0.710526315789473 0.472175622542595 0.019473684210527 0.0201834862385321],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1],...
    'BackgroundColor',[1 1 1]);

%% B1 Glenoid Correction
vec_B1Gl = figure (4)
%35°
v1 = quiver(u,v,B1_Gl_35(1),B1_Gl_35(2),0);
v1.LineWidth = LineWidth;
v1.MaxHeadSize = 0.2;

hold on

%60°
v2 = quiver(u,v,B1_Gl_60(1),B1_Gl_60(2),0);
v2.LineWidth = LineWidth;
% v2.MaxHeadSize = ArrowSize;


%75°
v3 = quiver(u,v,B1_Gl_75(1),B1_Gl_75(2),0);
v3.LineWidth = LineWidth;
% v3.MaxHeadSize = ArrowSize;


xO = 0.4;  %arrow length
yO = 0.2; % arrow width
arrows = patch([9-xO -yO; 9-xO +yO; 9 0],[yO 7-xO; -yO 7-xO; 0 7], 'k', 'clipping', 'off'); % set position of arrows, depends on Xlim and Ylim

%Options
grid off
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis equal ;
ax.XLim = [-7 9];
ax.YLim = [-9 7];
ax.XTick = [-6:2:8];
ax.YTick = [-8:2:6];


hold off

lgd = legend ([v1 v2 v3],'35°', '60°','75°','Location','southoutside','Orientation','horizontal', 'box','off','Position',[0.48 0.08 0 0]);
t = title('Dislocation Vectors B1 Glenoid Correction','FontSize',14, 'Position', [0 8 0]);
xlabel ('Posterior','FontWeight','bold');
ylabel ('Superior','FontWeight','bold');
set (gca, 'box', 'off');
set(gcf,'color','w'); % white background
set(gcf, 'Position', [10 10 700 700]);

%Mark dislocation

% Create textbox
annotation(vec_B1Gl,'textbox',...
    [0.656121705229256 0.304208402668355 0.0141578947368421 0.0201834862385321],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1],...
    'BackgroundColor',[1 1 1]);

% Create textbox
annotation(vec_B1Gl,'textbox',...
    [0.741835990943541 0.451351259811212 0.0141578947368421 0.0201834862385321],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1],...
    'BackgroundColor',[1 1 1]);
%% Di Lillo Plots

vec_DL = figure (5)
%35°
v1 = quiver(u,v,DL_35(1),DL_35(2),0);
v1.LineWidth = LineWidth;
v1.MaxHeadSize = 0.17;

hold on

%60°
v2 = quiver(u,v,DL_60(1),DL_60(2),0);
v2.LineWidth = LineWidth;
% v2.MaxHeadSize = ArrowSize;


%75°
v3 = quiver(u,v,DL_75(1),DL_75(2),0);
v3.LineWidth = LineWidth;
% v3.MaxHeadSize = ArrowSize;


xO = 0.4;  %arrow length
yO = 0.2; % arrow width
arrows = patch([13-xO -yO; 13-xO +yO; 13 0],[yO 7-xO; -yO 7-xO; 0 7], 'k', 'clipping', 'off'); % set position of arrows, depends on Xlim and Ylim

%Options
grid off
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis equal ;
ax.XLim = [-7 13];
ax.YLim = [-5 7];
ax.XTick = [-6:2:12];
ax.YTick = [-4:2:6];


hold off

lgd = legend ([v1 v2 v3],'35°', '60°','75°','Location','southoutside','Orientation','horizontal', 'box','off','Position',[0.43 0.2 0 0]);
t = title('Dislocation Vectors Di Lillo','FontSize',14, 'Position', [0 8 0]);
xlabel ('Posterior','FontWeight','bold');
ylabel ('Superior','FontWeight','bold');
set (gca, 'box', 'off');
set(gcf,'color','w'); % white background
set(gcf, 'Position', [10 10 700 700]);

%Mark dislocation


% Create textbox
annotation(vec_DL,'textbox',...
    [0.715939133368369 0.56622029415784 0.00771775082690251 0.00264200792602387],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_DL,'textbox',...
    [0.721653419082655 0.52622029415784 0.00771775082690251 0.00264200792602387],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_DL,'textbox',...
    [0.767367704796941 0.41622029415784 0.00771775082690251 0.00264200792602393],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);


%% 35° Vectors

vec_35 = figure (5) 


%b1
% v2 = quiver(u,v,B1_35(3),B1_35(4),0,'Color', [0 0.4470 0.7410]);
% v2.LineWidth = LineWidth;
% hold on
% v2.MaxHeadSize = 0.13;
v2ac = quiver(u,v, B1_35(1),B1_35(2),0,'Color', [0 0.4470 0.7410],'LineWidth',LineWidth);
v2ac.MaxHeadSize = 0.18;
hold on
v2end = quiver(B1_35(1),B1_35(2),B1_35(3)-B1_35(1),B1_35(4)-B1_35(2),0, 'Color', [0 0.4470 0.7410],'LineWidth',LineWidth);
v2end.MaxHeadSize = 0.6;

%B1 Acromion Corr
% v3 = quiver(u,v,B1_Ac_35(3),B1_Ac_35(4),0,'Color', [0.8500 0.3250 0.0980]);
% v3.LineWidth = LineWidth;
% v3.MaxHeadSize = 0.26;
v3ac = quiver(u,v, B1_Ac_35(1),B1_Ac_35(2),0,'Color', [0.8500 0.3250 0.0980],'LineWidth',LineWidth);
v3ac.MaxHeadSize = 0.28;
v3end = quiver(B1_Ac_35(1),B1_Ac_35(2),B1_Ac_35(3)-B1_Ac_35(1),B1_Ac_35(4)-B1_Ac_35(2),0, 'Color', [0.8500 0.3250 0.0980],'LineWidth',LineWidth);
v3end.MaxHeadSize = 0.7;

% B1 Glenoid Correction
% v4 = quiver(u,v,B1_Gl_35(3),B1_Gl_35(4),0,'Color', [0.9290 0.6940 0.1250]);
% v4.LineWidth = LineWidth;
% v4.MaxHeadSize = 0.22;
v4ac = quiver(u,v, B1_Gl_35(1),B1_Gl_35(2),0,'Color', [0.9290 0.6940 0.1250],'LineWidth',LineWidth);
v4ac.MaxHeadSize = 0.23;
v4end = quiver(B1_Gl_35(1),B1_Gl_35(2),B1_Gl_35(3)-B1_Gl_35(1),B1_Gl_35(4)-B1_Gl_35(2),0, 'Color', [0.9290 0.6940 0.1250],'LineWidth',LineWidth);
v4end.MaxHeadSize = 1.4;

% Di Lillo
% v5 = quiver(u,v,DL_35(3),DL_35(4),0,'Color', [0.4940 0.1840 0.5560]);
% v5.LineWidth = LineWidth;
% v5.MaxHeadSize = 0.15;
v5ac = quiver(u,v, DL_35(1),DL_35(2),0,'Color', [0.4940 0.1840 0.5560],'LineWidth',LineWidth);
v5ac.MaxHeadSize = 0.3;
v5end = quiver(DL_35(1),DL_35(2),DL_35(3)-DL_35(1),DL_35(4)-DL_35(2),0, 'Color', [0.4940 0.1840 0.5560],'LineWidth',LineWidth);
v5end.MaxHeadSize = 0.4;

%SSM
% v1 = quiver(u,v,SSM_35(3),SSM_35(4),0,'Color', [0.4660 0.6740 0.1880]);
% v1.LineWidth = LineWidth;
% v1.MaxHeadSize = 0.4;
v1ac = quiver(u,v, SSM_35(1),SSM_35(2),0,'Color', [0.4660 0.6740 0.1880], 'LineWidth',LineWidth);
v1ac.MaxHeadSize = 0.5;
v1end = quiver(SSM_35(1),SSM_35(2),SSM_35(3)-SSM_35(1),SSM_35(4)-SSM_35(2),0, 'Color', [0.4660 0.6740 0.1880],'LineWidth',LineWidth);
v1end.MaxHeadSize = 1.4;


xO = 0.4;  %arrow length
yO = 0.2; % arrow width
arrows = patch([12-xO -yO; 12-xO +yO; 12 0],[yO 7-xO; -yO 7-xO; 0 7], 'k', 'clipping', 'off'); % set position of arrows, depends on Xlim and Ylim

%Options
grid off
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis equal ;
ax.XLim = [-7 12];
ax.YLim = [-11 7];
ax.XTick = [-6:2:11];
ax.YTick = [-10:2:6];



hold off

lgd = legend ([v1ac v2ac v3ac v4ac v5ac],'SSM', 'B1','B1 Acromion Corr.','B1 Glenoid Corr.',...
'Di Lillo','Location','bestoutside','Orientation','horizontal', 'box','off','Position', [0.5 0.08 0 0]);
t = title('Dislocation Vectors at 35° GH','FontSize',14, 'Position', [0 8 0]);
xlabel ('Posterior','FontWeight','bold');
ylabel ('Superior','FontWeight','bold');
set (gca, 'box', 'off');
set(gcf,'color','w'); % white background
set(gcf, 'Position', [10 10 700 700]);

% mark dislocaiton
annotation(vec_35,'textbox',...
    [0.798796276225512 0.527648865586411 0.00771775082690251 0.00264200792602387],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);


%% 60° Vectors

vec_60 = figure (6)


%b1
%v2 = quiver(u,v,B1_60(3),B1_60(4),0,'Color', [0 0.4470 0.7410]);
% v2.LineWidth = LineWidth;
% v2.MaxHeadSize = 0.13;
%hold on
v2ac = quiver(u,v, B1_60(1),B1_60(2),0,'Color', [0 0.4470 0.7410],'LineWidth',LineWidth);
v2ac.MaxHeadSize = 0.25;
hold on
v2end = quiver(B1_60(1),B1_60(2),B1_60(3)-B1_60(1),B1_60(4)-B1_60(2),0, 'Color', [0 0.4470 0.7410],'LineWidth',LineWidth);
v2end.MaxHeadSize = 0.5;


%B1 Acromion Corr
%v3 = quiver(u,v,B1_Ac_60(3),B1_Ac_60(4),0,'Color', [0.8500 0.3250 0.0980]);
%v3.LineWidth = LineWidth;
%v3.MaxHeadSize = 0.26;
v3ac = quiver(u,v, B1_Ac_60(1),B1_Ac_60(2),0,'Color', [0.8500 0.3250 0.0980],'LineWidth',LineWidth);
v3ac.MaxHeadSize = 0.2;
v3end = quiver(B1_Ac_60(1),B1_Ac_60(2),B1_Ac_60(3)-B1_Ac_60(1),B1_Ac_60(4)-B1_Ac_60(2),0, 'Color', [0.8500 0.3250 0.0980],'LineWidth',LineWidth);
v3end.MaxHeadSize = 0.7;

% B1 Glenoid Correction
%v4 = quiver(u,v,B1_Gl_60(3),B1_Gl_60(4),0,'Color', [0.9290 0.6940 0.1250]);
% v4.LineWidth = LineWidth;
% v4.MaxHeadSize = 0.22;
v4ac = quiver(u,v, B1_Gl_60(1),B1_Gl_60(2),0,'Color', [0.9290 0.6940 0.1250],'LineWidth',LineWidth);
v4ac.MaxHeadSize = 0.25;
v4end = quiver(B1_Gl_60(1),B1_Gl_60(2),B1_Gl_60(3)-B1_Gl_60(1),B1_Gl_60(4)-B1_Gl_60(2),0, 'Color', [0.9290 0.6940 0.1250],'LineWidth',LineWidth);
v4end.MaxHeadSize = 0.6;

% Di Lillo
%v5 = quiver(u,v,DL_60(3),DL_60(4),0,'Color', [0.4940 0.1840 0.5560]);
% v5.LineWidth = LineWidth;
% v5.MaxHeadSize = 0.15;
v5ac = quiver(u,v, DL_60(1),DL_60(2),0,'Color', [0.4940 0.1840 0.5560],'LineWidth',LineWidth);
v5ac.MaxHeadSize = 0.2;
v5end = quiver(DL_60(1),DL_60(2),DL_60(3)-DL_60(1),DL_60(4)-DL_60(2),0, 'Color', [0.4940 0.1840 0.5560],'LineWidth',LineWidth);
v5end.MaxHeadSize = 0.2;

%SSM
%v1 = quiver(u,v,SSM_60(3),SSM_60(4),0,'Color', [0.4660 0.6740 0.1880]);
% v1.LineWidth = LineWidth;
% v1.MaxHeadSize = 0.4;
v1ac = quiver(u,v, SSM_60(1),SSM_60(2),0,'Color', [0.4660 0.6740 0.1880], 'LineWidth',LineWidth);
v1ac.MaxHeadSize = 0.22;
v1end = quiver(SSM_60(1),SSM_60(2),SSM_60(3)-SSM_60(1),SSM_60(4)-SSM_60(2),0, 'Color', [0.4660 0.6740 0.1880],'LineWidth',LineWidth);
v1end.MaxHeadSize = 0.22;

xO = 0.4;  %arrow length
yO = 0.2; % arrow width
arrows = patch([12-xO -yO; 12-xO +yO; 12 0],[yO 7-xO; -yO 7-xO; 0 7], 'k', 'clipping', 'off'); % set position of arrows, depends on Xlim and Ylim

%Options
grid off
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis equal ;
ax.XLim = [-7 12];
ax.YLim = [-11 7];
ax.XTick = [-6:2:11];
ax.YTick = [-10:2:6];


hold off

lgd = legend ([v1ac v2ac v3ac v4ac v5ac],'SSM', 'B1','B1 Acromion Corr.','B1 Glenoid Corr.',...
'Di Lillo','Location','bestoutside','Orientation','horizontal', 'box','off','Position', [0.5 0.08 0 0]);
t = title('Dislocation Vectors at 60° GH','FontSize',14, 'Position', [0 8 0]);
xlabel ('Posterior','FontWeight','bold');
ylabel ('Superior','FontWeight','bold');
set (gca, 'box', 'off');
set(gcf,'color','w'); % white background
set(gcf, 'Position', [10 10 700 700]);

% Mark dislocations

% Create textbox
annotation(vec_60,'textbox',...
    [0.750224847654083 0.687648865586411 0.00771775082690251 0.00264200792602387],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_60,'textbox',...
    [0.760224847654083 0.451934579872125 0.00771775082690251 0.00264200792602393],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_60,'textbox',...
    [0.638796276225512 0.529077437014983 0.00771775082690251 0.00264200792602387],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_60,'textbox',...
    [0.717367704796941 0.567648865586411 0.00771775082690263 0.00264200792602387],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

%% 75° Vectors

vec_75 = figure(7)


%b1
% v2 = quiver(u,v,B1_75(3),B1_75(4),0,'Color', [0 0.4470 0.7410]);
% v2.LineWidth = LineWidth;
% v2.MaxHeadSize = 0.13;
% hold on
v2ac = quiver(u,v, B1_75(1),B1_75(2),0,'Color', [0 0.4470 0.7410],'LineWidth',LineWidth);
v2ac.MaxHeadSize = 0.2;
hold on
v2end = quiver(B1_75(1),B1_75(2),B1_75(3)-B1_75(1),B1_75(4)-B1_75(2),0, 'Color', [0 0.4470 0.7410],'LineWidth',LineWidth);
v2end.MaxHeadSize = 0.5;


%B1 Acromion Corr
% v3 = quiver(u,v,B1_Ac_75(3),B1_Ac_75(4),0,'Color', [0.8500 0.3250 0.0980]);
% v3.LineWidth = LineWidth;
% v3.MaxHeadSize = 0.26;
v3ac = quiver(u,v, B1_Ac_75(1),B1_Ac_75(2),0,'Color', [0.8500 0.3250 0.0980],'LineWidth',LineWidth);
v3ac.MaxHeadSize = 0.13;
v3end = quiver(B1_Ac_75(1),B1_Ac_75(2),B1_Ac_75(3)-B1_Ac_75(1),B1_Ac_75(4)-B1_Ac_75(2),0, 'Color', [0.8500 0.3250 0.0980],'LineWidth',LineWidth);
v3end.MaxHeadSize = 0.4;

% B1 Glenoid Correction
% v4 = quiver(u,v,B1_Gl_75(3),B1_Gl_75(4),0,'Color', [0.9290 0.6940 0.1250]);
% v4.LineWidth = LineWidth;
% v4.MaxHeadSize = 0.22;
v4ac = quiver(u,v, B1_Gl_75(1),B1_Gl_75(2),0,'Color', [0.9290 0.6940 0.1250],'LineWidth',LineWidth);
v4ac.MaxHeadSize = 0.25;
v4end = quiver(B1_Gl_75(1),B1_Gl_75(2),B1_Gl_75(3)-B1_Gl_75(1),B1_Gl_75(4)-B1_Gl_75(2),0, 'Color', [0.9290 0.6940 0.1250],'LineWidth',LineWidth);
v4end.MaxHeadSize = 0.6;

% Di Lillo
% v5 = quiver(u,v,DL_75(3),DL_75(4),0,'Color', [0.4940 0.1840 0.5560]);
% v5.LineWidth = LineWidth;
% v5.MaxHeadSize = 0.15;
v5ac = quiver(u,v, DL_75(1),DL_75(2),0,'Color', [0.4940 0.1840 0.5560],'LineWidth',LineWidth);
v5ac.MaxHeadSize = 0.25;
v5end = quiver(DL_75(1),DL_75(2),DL_75(3)-DL_75(1),DL_75(4)-DL_75(2),0, 'Color', [0.4940 0.1840 0.5560],'LineWidth',LineWidth);
v5end.MaxHeadSize = 0.6;

%SSM
% v1 = quiver(u,v,SSM_75(3),SSM_75(4),0,'Color', [0.4660 0.6740 0.1880]);
% v1.LineWidth = LineWidth;
% v1.MaxHeadSize = 0.4;
v1ac = quiver(u,v, SSM_75(1),SSM_75(2),0,'Color', [0.4660 0.6740 0.1880], 'LineWidth',LineWidth);
v1ac.MaxHeadSize = 0.25;
v1end = quiver(SSM_75(1),SSM_75(2),SSM_75(3)-SSM_75(1),SSM_75(4)-SSM_75(2),0, 'Color', [0.4660 0.6740 0.1880],'LineWidth',LineWidth);
v1end.MaxHeadSize = 0.2;


xO = 0.4;  %arrow length
yO = 0.2; % arrow width
arrows = patch([12-xO -yO; 12-xO +yO; 12 0],[yO 7-xO; -yO 7-xO; 0 7], 'k', 'clipping', 'off'); % set position of arrows, depends on Xlim and Ylim

%Options
grid off
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
axis equal ;
ax.XLim = [-7 12];
ax.YLim = [-11 7];
ax.XTick = [-6:2:11];
ax.YTick = [-10:2:6];


hold off

lgd = legend ([v1ac v2ac v3ac v4ac v5ac],'SSM', 'B1','B1 Acromion Corr.','B1 Glenoid Corr.',...
'Di Lillo','Location','bestoutside','Orientation','horizontal', 'box','off','Position', [0.5 0.08 0 0]);
t = title('Dislocation Vectors at 75° GH','FontSize',14, 'Position', [0 8 0]);
xlabel ('Posterior','FontWeight','bold');
ylabel ('Superior','FontWeight','bold');
set (gca, 'box', 'off');
set(gcf,'color','w'); % white background
set(gcf, 'Position', [10 10 700 700]);

% Mark dislocation


% Create textbox
annotation(vec_75,'textbox',...
    [0.754510561939797 0.647648865586411 0.00771775082690251 0.00264200792602387],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_75,'textbox',...
    [0.840224847654083 0.529077437014982 0.00771775082690251 0.00264200792602387],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_75,'textbox',...
    [0.728796276225512 0.406220294157839 0.00771775082690251 0.00264200792602393],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_75,'textbox',...
    [0.688796276225511 0.243363151300697 0.00771775082690251 0.0026420079260239],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);

% Create textbox
annotation(vec_75,'textbox',...
    [0.528796276225511 0.391934579872125 0.00771775082690251 0.00264200792602393],...
    'String',{'*'},...
    'FontSize',30,...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);



